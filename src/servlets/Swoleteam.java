        import javax.servlet.ServletException;
        import javax.servlet.annotation.WebServlet;
        import javax.servlet.http.HttpServlet;
        import javax.servlet.http.HttpServletRequest;
        import javax.servlet.http.HttpServletResponse;
        import java.io.IOException;
        import java.io.PrintWriter;

@WebServlet(name = "Swoleteam", urlPatterns={"/Swoleteam"})
public class Swoleteam extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String bench = request.getParameter("bench");
        String squat = request.getParameter("squat");
	String deadlift = request.getParameter("deadlift");
        out.println("<h1>Welcome to the 1000 Pound Club</h1>");
        out.println("<p>Bench: " + bench + "</p>");
        out.println("<p>Squat: " + squat + "</p>");
	out.println("<p>Deadlift: " + deadlift + "</p>");
        out.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Please try again later.");
    }

}